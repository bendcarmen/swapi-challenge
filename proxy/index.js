import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cors from 'cors';

import peopleRouter from './routes/people.js';

const app = express();
const port = process.env.PORT || 3000;

console.log('starting server');

app.use(cors());
app.use(morgan('tiny'));
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);

app.use('/api/people', peopleRouter);

app.listen(port, () => {
  console.log(`listening at port ${port}`);
});
