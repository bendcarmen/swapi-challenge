import React from 'react';

import DetailRow from 'src/character/components/detailRow';

const Homeworld = ({ homeworld }) => (
  <DetailRow
    label="homeworld"
    value={homeworld}
  />
);

export default Homeworld;
