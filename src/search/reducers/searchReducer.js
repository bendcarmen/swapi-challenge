import {
  FETCH_MATCHING_CHARACTERS,
  FETCH_MATCHING_CHARACTERS__SUCCEEDED,
  FETCH_MATCHING_CHARACTERS__FAILED,
  CLEAR_ALL_CHARACTERS,
} from 'src/search/actions/searchActionTypes';

export const defaultState = {
  search: {
    results: [],
  },
  isFetching: false,
  errorData: null,
};

const searchReducer = (state = defaultState, action = {}) => {
  switch (action.type) {
    case FETCH_MATCHING_CHARACTERS: {
      return {
        ...defaultState,
        isFetching: true,
      };
    }

    case FETCH_MATCHING_CHARACTERS__SUCCEEDED: {
      const { results } = action;
      const search = {
        ...defaultState.search,
        results,
      };

      return {
        ...defaultState,
        search,
      };
    }

    case FETCH_MATCHING_CHARACTERS__FAILED: {
      const { errorData } = action;

      return {
        ...defaultState,
        errorData,
      };
    }

    case CLEAR_ALL_CHARACTERS: {
      return defaultState;
    }

    default: {
      return state;
    }
  }
};

export default searchReducer;
