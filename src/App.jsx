import React from 'react';
import { ConnectedRouter } from 'connected-react-router';

import routes from 'src/shared/routes';
import 'src/App.scss';

const App = ({ history }) => (
  <ConnectedRouter history={history}>
    {routes}
  </ConnectedRouter>
);

export default App;
