import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import DetailRow from 'src/character/components/detailRow';

describe('DetailRow', () => {
  it('should render an array of values as a comma delimited list', () => {
    const wrapper = shallow(<DetailRow
      label="value array"
      value={[{ name: 'value1' }, { name: 'value2' }, { name: 'value3' }]}
    />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should render a simple value', () => {
    const wrapper = shallow(<DetailRow
      label="simple value"
      value="I am just a lowly string"
    />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should render not available if a value cannot be parsed', () => {
    const wrapper = shallow(<DetailRow
      label="no value provided"
    />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
