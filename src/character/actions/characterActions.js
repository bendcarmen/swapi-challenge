import { baseUrl } from 'src/shared/constants/apiConstants';
import {
  fetchCharacterDetails,
  fetchCharacterDetailsSucceeded,
  fetchCharacterDetailsFailed,
} from 'src/character/actions/characterActionTypes';

export const getCharacterDetails = (requestData) => async (dispatch) => {
  const url = `${baseUrl}/people/details`;
  const headers = {
    'Content-Type': 'application/json',
  };

  dispatch(fetchCharacterDetails());

  try {
    const response = await fetch(url, {
      method: 'POST',
      headers,
      body: JSON.stringify(requestData),
    });

    if (response.ok === true) {
      const results = await response.json();

      dispatch(fetchCharacterDetailsSucceeded(results));
    } else {
      dispatch(fetchCharacterDetailsFailed(response));
    }
  } catch (err) {
    dispatch(fetchCharacterDetailsFailed({
      ok: false,
      message: err,
      code: 404,
    }));
  }
};
