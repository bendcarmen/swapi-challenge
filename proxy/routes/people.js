import express from 'express';
import fetch from 'node-fetch';
import _ from 'lodash';

import { apiBaseUrl } from '../constants/proxyConstants.js';

const peopleRouter = express.Router();

const getOne = async (url) => {
  try {
    const response = await fetch(url, {
      method: 'GET',
    });
    const { ok, status } = response;
    if (response.ok === true) {
      const result = await response.json();
      return {
        ...result,
        ok,
        status,
      };
    }
    return {
      ok,
      status: response.status,
      results: [],
      count: 0,
    };
  } catch (e) {
    return {
      ok: false,
      results: [],
      count: 0,
    };
  }
};

const getAll = async (urls) => (
  await Array.isArray(urls) && urls.length > 0
    ? Promise.all(
      urls.map((filmUrl) => getOne(filmUrl)
        .then((result) => result)),
    )

    : []);

peopleRouter.route('/find').get(async (req, res) => {
  const { search } = req.query;

  if (!search || search.length < 3) {
    return [];
  }

  const startPage = 1;
  const { results: page1Results, count } = await getOne(`${apiBaseUrl}/people?page=${startPage}&search=${search}`);

  const totalPages = (
    count > 0
    && page1Results.length > 0
    && Math.ceil(count / page1Results.length)
  ) || 0;

  if (totalPages <= 1) {
    return res.json(page1Results);
  }

  const allPeople = (
    await Promise.all(
      _.range(2, totalPages + 1).map((pageNumber) => getOne(`${apiBaseUrl}/people?page=${pageNumber}&search=${search}`))
        .then((result) => result),
    )
      .filter((result) => result.ok)
      .flatMap((result) => result.results));

  res.json(allPeople);
});

peopleRouter.route('/details').post(async (req, res) => {
  const {
    films: filmUrls, homeworld: homeWorldUrl, species: speciesUrls, starships: starshipUrls,
  } = req.body;

  const filmList = await getAll(filmUrls);
  const homeworld = await getOne(homeWorldUrl);
  const speciesList = await getAll(speciesUrls);
  const starshipList = await getAll(starshipUrls);

  res.json({
    filmList,
    homeworld,
    speciesList,
    starshipList,
  });
});

export default peopleRouter;
