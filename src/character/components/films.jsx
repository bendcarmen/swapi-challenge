import React from 'react';

import DetailRow from 'src/character/components/detailRow';

const Films = ({ filmList }) => {
  const value = filmList.map(({ title: name }) => ({ name }));

  return (
    <DetailRow
      label="appeared in"
      value={value}
    />
  );
};

export default Films;
