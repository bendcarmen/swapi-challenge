import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { selectedCharacter } from 'src/mock/mockSelectedCharacter';
import Starships from 'src/character/components/starships';

describe('Starships', () => {
  it('should render', () => {
    const wrapper = shallow(<Starships starshipList={selectedCharacter.starshipList} />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
