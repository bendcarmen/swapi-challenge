import { baseUrl } from 'src/shared/constants/apiConstants';
import {
  fetchMatchingCharacters,
  fetchMatchingCharactersFailed,
  fetchMatchingCharactersSucceeded,
  CLEAR_ALL_CHARACTERS,
} from 'src/search/actions/searchActionTypes';

export const getCharacters = (search) => async (dispatch) => {
  const url = `${baseUrl}/people/find?search=${search}`;

  dispatch(fetchMatchingCharacters());

  try {
    const response = await fetch(url, {
      method: 'GET',
    });
    if (response.ok === true) {
      const results = await response.json();

      dispatch(fetchMatchingCharactersSucceeded(results));
    } else {
      dispatch(fetchMatchingCharactersFailed(response));
    }
  } catch (e) {
    dispatch(fetchMatchingCharactersFailed({
      ok: false,
      message: 'uknownError',
      code: 404,
    }));
  }
};

export const clearAllCharacters = () => ({
  type: CLEAR_ALL_CHARACTERS,
});
