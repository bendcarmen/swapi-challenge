import {
  CHARACTER_SELECTED,
} from 'src/shared/actions/sharedActionTypes';

import {
  FETCH_CHARACTER_DETAILS,
  FETCH_CHARACTER_DETAILS__SUCCEEDED,
  FETCH_CHARACTER_DETAILS__FAILED,
} from 'src/character/actions/characterActionTypes';

export const defaultState = {
  character: {
    filmList: null,
    homeworld: null,
    speciesList: null,
    starshipList: null,
    biometrics: null,
  },
  isFetching: false,
  errorData: null,
};

const characterReducer = (state = defaultState, action = {}) => {
  switch (action.type) {
    case CHARACTER_SELECTED: {
      const { characterData: biometrics } = action;
      const character = {
        ...defaultState.character,
        biometrics,
      };

      return {
        ...state,
        character,
      };
    }

    case FETCH_CHARACTER_DETAILS: {
      const result = {
        ...defaultState,
        isFetching: true,
      };

      return result;
    }

    case FETCH_CHARACTER_DETAILS__SUCCEEDED: {
      const character = {
        ...state.character,
        ...action.response,
      };

      return {
        ...defaultState,
        character,
      };
    }

    case FETCH_CHARACTER_DETAILS__FAILED: {
      const { errorData } = action;

      return {
        ...defaultState,
        errorData,
      };
    }

    default: {
      return state;
    }
  }
};

export default characterReducer;
