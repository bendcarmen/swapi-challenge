import React, { useState, useEffect } from 'react';
import {
  Autocomplete,
  TextField,
  CircularProgress,
} from '@mui/material';

import { minimumSearchLength } from 'src/search/constants/searchBarConstants';

const SearchBar = ({
  options,
  onChange,
  showLoader,
  onSelected,
}) => {
  const [inputText, setInputText] = useState('');
  const [searchKey, setSearchKey] = useState('');
  const renderedOptions = inputText.length < minimumSearchLength || showLoader ? [] : options;
  const showOptions = inputText.length >= minimumSearchLength && !showLoader;

  useEffect(() => {
    if (searchKey.length === minimumSearchLength) {
      onChange(searchKey);
    }
  }, [searchKey]);

  useEffect(() => {
    if (inputText.length === minimumSearchLength && inputText !== searchKey) {
      setSearchKey(inputText);
    }
  }, [inputText]);

  const handleSearchChange = (event, value) => {
    setInputText(value);
  };

  const handleOnChange = (event, value) => {
    onSelected(value);
  };

  return (
    <div className="search-bar">
      <Autocomplete
        open={showOptions}
        blurOnSelect
        disablePortal
        id="combo-box-demo"
        options={renderedOptions}
        getOptionLabel={({ name }) => name}
        onChange={handleOnChange}
        onInputChange={handleSearchChange}
        noOptionsText="no matches found"
        sx={{
          width: '300px',
          paddingLeft: 'calc(50% - 150px)',
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Search for a Star Wars Character"
            variant="outlined"
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <>
                  {showLoader
                    ? <CircularProgress color="inherit" size={20} />
                    : null}
                  {params.InputProps.endAdornment}
                </>
              ),
            }}
          />
        )}
      />
    </div>
  );
};

export default SearchBar;
