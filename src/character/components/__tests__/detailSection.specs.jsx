import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import DetailSection from 'src/character/components/detailSection';

describe('DetailSection', () => {
  it('should render a map into an array of DetailRows', () => {
    const detailMap = new Map([
      ['label one', 'value one'],
      ['label two', 'value two'],
      ['label three', 'value thre'],
    ]);

    const wrapper = shallow(<DetailSection detailMap={detailMap} />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
