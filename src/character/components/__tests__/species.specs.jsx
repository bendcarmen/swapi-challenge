import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { selectedCharacter } from 'src/mock/mockSelectedCharacter';
import Species from 'src/character/components/species';

describe('Species', () => {
  it('should render', () => {
    const wrapper = shallow(<Species Homeworld={selectedCharacter.species} />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
