import React from 'react';

import DetailRow from 'src/character/components/detailRow';

const DetailSection = ({ detailMap }) => (
  <>
    {Array.from(detailMap).map(
      ([label, value]) => (
        <DetailRow
          key={`${label}-${value}`}
          label={label}
          value={value}
        />
      ),
    )}
  </>
);

export default DetailSection;
