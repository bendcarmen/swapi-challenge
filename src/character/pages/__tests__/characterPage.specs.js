import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';

import { selectedCharacter } from 'src/mock/mockSelectedCharacter';
import { CharacterPage } from 'src/character/pages/characterPage';

describe('CharacterPage', () => {
  it('should render a selected character', () => {
    const props = {
      character: selectedCharacter,
      showLoader: false,
      searchAgainFn: jest.fn(),
    };
    const wrapper = shallow(<CharacterPage {...props} />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should show a loading indicator if showLoader is true', () => {
    const props = {
      character: selectedCharacter,
      showLoader: true,
      searchAgainFn: jest.fn(),
    };
    const wrapper = shallow(<CharacterPage {...props} />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should call searchAgainFn if the Search Again button is clicked', () => {
    const props = {
      character: selectedCharacter,
      showLoader: false,
      searchAgainFn: jest.fn(),
    };
    const wrapper = mount(<CharacterPage {...props} />);

    wrapper.find('button[type="submit"]').simulate('click');

    expect(props.searchAgainFn).toHaveBeenCalled();
  });
});
