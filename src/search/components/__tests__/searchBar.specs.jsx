import React from 'react';
import { mount, shallow } from 'enzyme';

import SearchBar from 'src/search/components/searchBar';
import { at } from 'lodash';

describe('searchBar', () => {
  let props;
  beforeEach(() => {
    props = {
      options: [{
        name: 'mock name',
      }],
      onChange: jest.fn(),
      showLoader: false,
      onSelected: jest.fn(),
    };
  });

  it('should render', () => {
    const wrapper = shallow(<SearchBar {...props} />);

    expect(wrapper).toMatchSnapshot();
  });

  xit('should call on change', () => {
    const wrapper = mount(<SearchBar {...props} />);
    const muiDebounceMs = 500;
    // -- input onChange not firing in MUI autocomplete not working TODO: use redux dev tools -- //
    wrapper.at(0).find('input.MuiAutocomplete-input').at(0).simulate('change');
    expect(props.onChange).toHaveBeenCalledWith({});
  });
});
