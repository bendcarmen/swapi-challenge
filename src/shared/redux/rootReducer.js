import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import searchReducer from 'src/search/reducers/searchReducer';
import characterReducer from 'src/character/reducers/characterReducer';

const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  search: searchReducer,
  character: characterReducer,
});

export default createRootReducer;
