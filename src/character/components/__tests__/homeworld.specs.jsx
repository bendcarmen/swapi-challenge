import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { selectedCharacter } from 'src/mock/mockSelectedCharacter';
import Homeworld from 'src/character/components/homeworld';

describe('Homeworld', () => {
  it('should render', () => {
    const wrapper = shallow(<Homeworld Homeworld={selectedCharacter.homeworld} />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
