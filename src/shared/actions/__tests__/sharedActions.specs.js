import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { push } from 'connected-react-router';

import { characterSelected } from 'src/shared/actions/sharedActionTypes';
import { fetchCharacterDetails } from 'src/character/actions/characterActionTypes';
import { selectCharacter } from 'src/shared/actions/sharedActions';

const middleware = [thunk];
const mockStore = configureStore(middleware);
const store = mockStore();

describe('selectCharacter', () => {
  beforeEach(() => {
    store.clearActions();
  });

  it('should dispatch fetchCharacterDetails, characterSelected actions and then push the /character route', async () => {
    const mockCharacterData = {
      films: ['mock films list'],
      homeworld: 'mock home world',
      species: ['mock species list'],
      starships: ['mock starships list'],
    };
    const expectedResult = [
      fetchCharacterDetails(),
      characterSelected(mockCharacterData),
      push('/character'),
    ];

    await store.dispatch(selectCharacter(mockCharacterData));

    expect(store.getActions()[0]).toEqual(expectedResult[0]);
  });
});
