import characterReducer, { defaultState } from 'src/character/reducers/characterReducer';

describe('characterReducer', () => {
  it('should render default state with an uknown action', () => {
    const result = characterReducer(defaultState, {
      type: 'UKNOWN_ACTION_TYPE',
    });

    expect(result).toEqual(defaultState);
  });

  it('CHARACTER_SELECTED should set the biometric data', () => {
    const mockCharacterData = 'mock biometric data';
    const result = characterReducer(defaultState, {
      type: 'CHARACTER_SELECTED',
      characterData: mockCharacterData,
    });
    const character = {
      ...defaultState.character,
      biometrics: mockCharacterData,
    };
    const expectedResult = {
      ...defaultState,
      character,
    };

    expect(result).toEqual(expectedResult);
  });

  it('FETCH_CHARACTER_DETAILS should set isFetching to true', () => {
    const expectedResult = {
      ...defaultState,
      isFetching: true,
    };
    const result = characterReducer(defaultState, {
      type: 'FETCH_CHARACTER_DETAILS',
    });

    expect(result).toEqual(expectedResult);
  });

  it('FETCH_CHARACTER_DETAILS__SUCCEEDED should add the response to the character property', () => {
    const mockReturnVal = 'mock character details';
    const expectedResult = {
      character: {
        ...defaultState.character,
        mockReturnVal,
      },
      isFetching: defaultState.isFetching,
      errorData: defaultState.errorData,
    };

    const result = characterReducer(defaultState, {
      type: 'FETCH_CHARACTER_DETAILS__SUCCEEDED',
      response: { mockReturnVal },
    });

    expect(result).toEqual(expectedResult);
  });

  it('FETCH_CHARACTER_DETAILS__FAILED should return errorData', () => {
    const mockErrorData = 'mock error data';
    const expectedResult = {
      ...defaultState,
      errorData: mockErrorData,
    };
    const result = characterReducer(defaultState, {
      type: 'FETCH_CHARACTER_DETAILS__FAILED',
      errorData: mockErrorData
    });

    expect(result).toEqual(expectedResult);
  });
});
