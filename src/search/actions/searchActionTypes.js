export const FETCH_MATCHING_CHARACTERS = 'FETCH_MATCHING_CHARACTERS';
export const FETCH_MATCHING_CHARACTERS__SUCCEEDED = 'FETCH_MATCHING_CHARACTERS__SUCCEEDED ';
export const FETCH_MATCHING_CHARACTERS__FAILED = 'FETCH_MATCHING_CHARACTERS__FAILED';
export const CLEAR_ALL_CHARACTERS = 'CLEAR_ALL_CHARACTERS';

// -- asyc -- //
export const fetchMatchingCharacters = () => ({
  type: FETCH_MATCHING_CHARACTERS,
});

export const fetchMatchingCharactersFailed = (errorData) => ({
  type: FETCH_MATCHING_CHARACTERS__FAILED,
  errorData,
});

export const fetchMatchingCharactersSucceeded = (results) => ({
  type: FETCH_MATCHING_CHARACTERS__SUCCEEDED,
  results,
});
