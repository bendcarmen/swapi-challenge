import React from 'react';

import 'src/character/components/detailRow.scss';

const DetailRow = ({ label, value }) => {
  const renderValue = Array.isArray(value) && value.length > 0
    && value.map(({ name }) => name).join(', ')
    || value && !Array.isArray(value) && value
    || 'not available';

  return (
    <div className="character-page--detail-row">
      <div className="character-page--detail-row-label">{label}:</div>
      <div>{renderValue}</div>
    </div>
  );
};

export default DetailRow;
