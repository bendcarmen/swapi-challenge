import {
  homeworld,
  films as filmList,
  species,
  starships as starshipList,
  characters,
} from './luke-mock.js';

const [biometrics] = characters;

export const selectedCharacter = {
  speciesList: [species],
  filmList,
  starshipList,
  biometrics,
  homeworld,
};
