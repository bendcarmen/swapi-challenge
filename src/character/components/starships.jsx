import React from 'react';

import DetailRow from 'src/character/components/detailRow';

const Starships = ({ starshipList }) => (
  <DetailRow
    label="starships"
    value={starshipList}
  />
);

export default Starships;
