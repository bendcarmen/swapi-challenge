import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { baseUrl } from 'src/shared/constants/apiConstants';
import {
  fetchCharacterDetails,
  fetchCharacterDetailsSucceeded,
  fetchCharacterDetailsFailed,
} from 'src/character/actions/characterActionTypes';
import { getCharacterDetails } from 'src/character/actions/characterActions';

const middleware = [thunk];
const mockStore = configureStore(middleware);
const store = mockStore();

describe('getCharacterDetails', () => {
  beforeEach(() => {
    store.clearActions();
  });
  describe('getCharacterDetails success path', () => {
    it('should dispatch fetchCharacterDetails, make the api call, then dispatch fetchCharacterDetailsSucceeded', async () => {
      const mockBody = 'mock body';
      const mockResponse = ['mock character details'];
      const fetchMock = jest
        .spyOn(global, 'fetch')
        .mockImplementation(() => Promise.resolve({
          ok: true,
          json: () => Promise.resolve(mockResponse),
        }));
      const expectedResult = [
        fetchCharacterDetails(),
        fetchCharacterDetailsSucceeded(mockResponse),
      ];

      await store.dispatch(getCharacterDetails(mockBody));

      expect(fetchMock).toHaveBeenCalledWith(`${baseUrl}/people/details`, {
        body: JSON.stringify(mockBody),
        headers: { 'Content-Type': 'application/json' },
        method: 'POST',
      });

      expect(store.getActions()).toEqual(expectedResult);
    });
  });

  describe('getCharacterDetails failure path', () => {
    it('should dispatch fetchCharacterDetails, make the api call, then dispatch fetchCharacterDetailsFailed', async () => {
      const mockBody = 'mock body';
      const mockResponse = {
        message: 'mock fail',
        ok: false,
      };
      const fetchMock = jest
        .spyOn(global, 'fetch')
        .mockImplementation(() => Promise.resolve({
          ok: false,
          message: mockResponse.message,
        }));
      const expectedResult = [
        fetchCharacterDetails(),
        fetchCharacterDetailsFailed(mockResponse),
      ];

      await store.dispatch(getCharacterDetails(mockBody));

      expect(fetchMock).toHaveBeenCalledWith(`${baseUrl}/people/details`, {
        body: JSON.stringify(mockBody),
        headers: { 'Content-Type': 'application/json' },
        method: 'POST',
      });

      expect(store.getActions()).toEqual(expectedResult);
    });
  });
});
