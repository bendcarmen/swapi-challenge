import React from 'react';

import DetailRow from 'src/character/components/detailRow';

const Species = ({ speciesList }) => (
  <DetailRow
    label="species"
    value={speciesList}
  />
);

export default Species;
