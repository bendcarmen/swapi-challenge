export const FETCH_CHARACTER_DETAILS = 'FETCH_CHARACTER_DETAILS';
export const FETCH_CHARACTER_DETAILS__SUCCEEDED = 'FETCH_CHARACTER_DETAILS__SUCCEEDED';
export const FETCH_CHARACTER_DETAILS__FAILED = 'FETCH_CHARACTER_DETAILS__FAILED';

export const fetchCharacterDetails = () => ({
  type: FETCH_CHARACTER_DETAILS,
});

export const fetchCharacterDetailsSucceeded = (response) => ({
  type: FETCH_CHARACTER_DETAILS__SUCCEEDED,
  response,
});

export const fetchCharacterDetailsFailed = (errorData) => ({
  type: FETCH_CHARACTER_DETAILS__FAILED,
  errorData,
});
