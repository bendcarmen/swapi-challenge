import React from 'react';
import { connect } from 'react-redux';

import { getCharacters } from 'src/search/actions/searchActions';
import { selectCharacter } from 'src/shared/actions/sharedActions';

import 'src/search/pages/searchPage.scss';
import SearchBar from 'src/search/components/searchBar';

export const SearchPage = ({
  search,
  getCharactersFn,
  characterSelectedFn,
  isFetching,
}) => {
  const { results } = search;

  return (
    <div className="search-page">
      <SearchBar
        options={results}
        onChange={getCharactersFn}
        onSelected={characterSelectedFn}
        showLoader={isFetching}
      />
      <p>
        Before using, please verify that <a href="https://swapi.dev/" target="_blank" rel="noreferrer">SWAPI</a> is up
      </p>
    </div>
  );
};

const mapStateToProps = (state) => ({
  search: state.search.search,
  isFetching: state.search.isFetching,
});

const mapDispatchToProps = (dispatch) => ({
  getCharactersFn: (key) => dispatch(getCharacters(key)),
  characterSelectedFn: (characterData) => dispatch(selectCharacter(characterData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
