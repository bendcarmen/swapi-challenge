import { push } from 'connected-react-router';

import { characterSelected } from 'src/shared/actions/sharedActionTypes';
import { getCharacterDetails } from 'src/character/actions/characterActions';

export const selectCharacter = (characterData) => (dispatch) => {
  const {
    films, homeworld, species, starships,
  } = characterData;

  const requestData = {
    films,
    homeworld,
    species,
    starships,
  };

  dispatch(getCharacterDetails(requestData));
  dispatch(characterSelected(characterData));
  dispatch(push('/character'));
};
