import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { selectedCharacter } from 'src/mock/mockSelectedCharacter';
import Films from 'src/character/components/films';

describe('Films', () => {
  it('should render', () => {
    const wrapper = shallow(<Films filmList={selectedCharacter.filmList} />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
