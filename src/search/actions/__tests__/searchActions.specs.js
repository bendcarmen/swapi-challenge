import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { baseUrl } from 'src/shared/constants/apiConstants';
import {
  fetchMatchingCharacters,
  fetchMatchingCharactersFailed,
  fetchMatchingCharactersSucceeded,
} from 'src/search/actions/searchActionTypes';
import { getCharacters } from 'src/search/actions/searchActions';

const middleware = [thunk];
const mockStore = configureStore(middleware);
const store = mockStore();

describe('getCharacters', () => {
  beforeEach(() => {
    store.clearActions();
  });

  describe('getCharacters success path', () => {
    it('should dispatch fetchMatchingCharacters, make the api call, then dispatch fetchMatchingCharactersSucceeded', async () => {
      const mockSearch = undefined;
      const mockResponse = ['mock character matches'];
      const fetchMock = jest
        .spyOn(global, 'fetch')
        .mockImplementation(() => Promise.resolve({
          ok: true,
          json: () => Promise.resolve(mockResponse),
        }));
      const expectedResult = [
        fetchMatchingCharacters(),
        fetchMatchingCharactersSucceeded(mockResponse),
      ];

      await store.dispatch(getCharacters());

      expect(fetchMock).toHaveBeenCalledWith(`${baseUrl}/people/find?search=${mockSearch}`, {
        method: 'GET',
      });

      expect(store.getActions()).toEqual(expectedResult);
    });
  });

  describe('getCharacters failure path', () => {
    it('should dispatch fetchMatchingCharacters, make the api call, then dispatch fetchMatchingCharactersFailed', async () => {
      const mockSearch = undefined;
      const mockResponse = {
        message: 'mock fail',
        ok: false,
      };
      const fetchMock = jest
        .spyOn(global, 'fetch')
        .mockImplementation(() => Promise.resolve({
          ok: false,
          message: mockResponse.message,
        }));
      const expectedResult = [
        fetchMatchingCharacters(),
        fetchMatchingCharactersFailed(mockResponse),
      ];

      await store.dispatch(getCharacters());

      expect(fetchMock).toHaveBeenCalledWith(`${baseUrl}/people/find?search=${mockSearch}`, {
        method: 'GET',
      });

      expect(store.getActions()).toEqual(expectedResult);
    });
  });
});
