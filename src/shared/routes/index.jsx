import React from 'react';
import { Route, Switch } from 'react-router';

import SearchPage from 'src/search/pages/searchPage';
import CharacterPage from 'src/character/pages/characterPage';

const routes = (
  <div>
    <Switch>
      <Route exact path="/" component={SearchPage} />
      <Route path="/character" component={CharacterPage} />
    </Switch>
  </div>
);

export default routes;
