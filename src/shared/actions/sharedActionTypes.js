export const CHARACTER_SELECTED = 'CHARACTER_SELECTED';

export const characterSelected = (characterData) => ({
  type: CHARACTER_SELECTED,
  characterData,
});
