import React from 'react';

import DetailSection from 'src/character/components/detailSection';

const Biometrics = ({ biometrics }) => {
  const detailMap = new Map([
    ['height', biometrics.height],
    ['weight', biometrics.mass],
    ['hair color', biometrics.hair_color],
    ['skin color', biometrics.skin_color],
    ['birth year', biometrics.birth_year],
  ]);

  return (
    <DetailSection
      detailMap={detailMap}
    />
  );
};

export default Biometrics;
