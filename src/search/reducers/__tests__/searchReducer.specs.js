import searchReducer, { defaultState } from 'src/search/reducers/searchReducer';
import {
  FETCH_MATCHING_CHARACTERS,
  FETCH_MATCHING_CHARACTERS__SUCCEEDED,
  FETCH_MATCHING_CHARACTERS__FAILED,
} from 'src/search/actions/searchActionTypes';

describe('searchReducer', () => {
  it('should render default state with an uknown action', () => {
    const result = searchReducer(defaultState, {
      type: 'UKNOWN_ACTION_TYPE',
    });

    expect(result).toEqual(defaultState);
  });

  it('FETCH_MATCHING_CHARACTERS should set isFetching to true', () => {
    const expectedResult = {
      ...defaultState,
      isFetching: true,
    };
    const result = searchReducer(defaultState, {
      type: FETCH_MATCHING_CHARACTERS,
    });

    expect(result).toEqual(expectedResult);
  });

  it('FETCH_MATCHING_CHARACTERS__SUCCEEDED should return a list of results', () => {
    const search = {
      ...defaultState.search,
      results: ['mock result list'],
    };
    const result = searchReducer(defaultState, {
      type: FETCH_MATCHING_CHARACTERS__SUCCEEDED,
      results: search.results,
    });

    const expectedResult = {
      ...defaultState,
      search,
    };

    expect(result).toEqual(expectedResult);
  });

  it('FETCH_MATCHING_CHARACTERS__FAILED should return error data', () => {
    const mockErrorData = 'mock error data';
    const result = searchReducer(defaultState, {
      type: FETCH_MATCHING_CHARACTERS__FAILED,
      errorData: mockErrorData,
    });
    const expectedResult = {
      ...defaultState,
      errorData: mockErrorData,
    };

    expect(result).toEqual(expectedResult);
  });
});
