import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { selectedCharacter } from 'src/mock/mockSelectedCharacter';
import Biometrics from 'src/character/components/biometrics';

describe('Biometrics', () => {
  it('should render', () => {
    const wrapper = shallow(<Biometrics biometrics={selectedCharacter.biometrics} />);

    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
