import React from 'react';
import { connect } from 'react-redux';
import { Button } from '@mui/material';
import { push } from 'connected-react-router';
import Loader from 'react-loader-spinner';

import Biometrics from 'src/character/components/biometrics';
import Species from 'src/character/components/species';
import Starships from 'src/character/components/starships';
import Homeworld from 'src/character/components/homeworld';
import Films from 'src/character/components/films';
import 'src/character/pages/characterPage.scss';

export const CharacterPage = ({ character, showLoader, searchAgainFn }) => (
  <div className="character-page">
    {showLoader
      ? <Loader type="ThreeDots" color="#1976d2" height="100" width="100" />
      : (
        <>
          <h1>{character.biometrics.name}</h1>
          <Biometrics
            biometrics={character.biometrics}
          />
          <Species
            speciesList={character.speciesList}
          />
          <Starships
            starshipList={character.starshipList}
          />
          <Homeworld
            homeworld={character.homeworld.name}
          />
          <Films
            filmList={character.filmList}
          />
        </>
      )}

    <Button
      variant="outlined"
      size="small"
      type="submit"
      sx={{
        width: '20rem',
      }}
      onClick={searchAgainFn}
    >
      Search Again
    </Button>
  </div>
);

const mapDispatchToProps = (dispatch) => ({
  searchAgainFn: () => dispatch(push('/')),
});

const mapStateToProps = (state) => ({
  character: state.character.character,
  showLoader: state.character.isFetching,
});

export default connect(mapStateToProps, mapDispatchToProps)(CharacterPage);
